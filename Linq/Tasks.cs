﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linq.Objects;
using MoralesLarios.Linq;

namespace Linq
{
    public static class Tasks
    {
        //The implementation of your tasks should look like this:
        public static string TaskExample(IEnumerable<string> stringList)
        {
            return stringList.Aggregate<string>((x, y) => x + y);
        }

        #region Low

        public static IEnumerable<string> Task1(char c, IEnumerable<string> stringList)
        {
            return stringList.Where(str=> str.StartsWith(c) && str.EndsWith(c) && str.Length > 1);
        }

        public static IEnumerable<int> Task2(IEnumerable<string> stringList)
        {
            return stringList.OrderBy(str => str.Length).Select(str => str.Length);
        }

        public static IEnumerable<string> Task3(IEnumerable<string> stringList)
        {
            return stringList.Select(str=>str[0].ToString() + str[str.Length-1]);
        }

        public static IEnumerable<string> Task4(int k, IEnumerable<string> stringList)
        {
            return stringList.Where(str => str.Length == k && Char.IsDigit(str[str.Length - 1])).OrderBy(str => str);
        }

        public static IEnumerable<string> Task5(IEnumerable<int> integerList)
        {
            return integerList.Where(num => num % 2 == 1).OrderBy(num => num).Select(num=>num.ToString());
        }

        #endregion

        #region Middle

        public static IEnumerable<string> Task6(IEnumerable<int> numbers, IEnumerable<string> stringList)
        {
            return stringList.Where(str => char.IsDigit(str.FirstOrDefault())).RightJoin<string, string, string, string>(
                numbers.Select(num => num.ToString()),
                str => str.Length.ToString(),
                num => num.ToString(),
                (str, num) =>
                {
                    if (str is null) str = "Not found";
                    str = str.Length.ToString() == num ? str : "Not found";
                    return str.ToString();
                })
                .Distinct(new StringEqualityComparer());
        }

        class StringEqualityComparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                if (x.Length == y.Length)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(string obj)
            {
                if (obj == "Not found")
                    return obj.GetHashCode() + new Random().Next(int.MinValue, int.MaxValue);
                else
                    return obj.Length.GetHashCode();
            }
        }

        public static IEnumerable<int> Task7(int k, IEnumerable<int> integerList)
        {
            return (from n in integerList where n % 2 == 0 select n)
                .Except(integerList.Skip(k)).Reverse();
        }
        
        public static IEnumerable<int> Task8(int k, int d, IEnumerable<int> integerList)
        {
            return integerList.TakeWhile(num => num <= d)
                .Union(integerList.Skip(k))
                .OrderByDescending(num=>num);
        }

        public static IEnumerable<string> Task9(IEnumerable<string> stringList)
        {
            return stringList.GroupBy(str => str.FirstOrDefault())
                .Select(gr => gr.Sum(item=>item.Length).ToString() + "-" + gr.Key.ToString())
                .OrderByDescending(result=>int.Parse(result.Substring(0, result.IndexOf("-"))))
                .ThenBy(result => result[^1]);
        }

        public static IEnumerable<string> Task10(IEnumerable<string> stringList)
        {
            return stringList.OrderBy(c => c).GroupBy(i => i.Length, str => str[^1].ToString().ToUpper())
                .Select(s => s.Aggregate((a, b) => a + b)).OrderByDescending(c=>c.Length);
        }

        #endregion

        #region Advance

        public static IEnumerable<YearSchoolStat> Task11(IEnumerable<Entrant> nameList)
        {
            return nameList.GroupBy((x => x.Year), x => x.SchoolNumber)
                .Select(x => new YearSchoolStat
                {
                    NumberOfSchools = x.Count() - 1,
                    Year = x.Key
                })
                .OrderBy(x => x.NumberOfSchools)
                .ThenBy(x => x.Year)
                .Select(x => new YearSchoolStat
                {
                    NumberOfSchools = x.NumberOfSchools,
                    Year = x.Year
                });
        }

        public static IEnumerable<NumberPair> Task12(IEnumerable<int> integerList1, IEnumerable<int> integerList2)
        {
            return new List<NumberPair>();
        }

        public static IEnumerable<YearSchoolStat> Task13(IEnumerable<Entrant> nameList, IEnumerable<int> yearList)
        {
            return new List<YearSchoolStat>();
        }

        public static IEnumerable<MaxDiscountOwner> Task14(IEnumerable<Supplier> supplierList,
                IEnumerable<SupplierDiscount> supplierDiscountList)
        {
            return new List<MaxDiscountOwner>();

        }

        public static IEnumerable<CountryStat> Task15(IEnumerable<Good> goodList,
            IEnumerable<StorePrice> storePriceList)
        {
            return goodList.GroupJoin(
                storePriceList,
                g => g.Id,
                s => s.GoodId,
                (good, store) => new CountryStat
                {
                    Country = good.Country,
                    MinPrice = store.Min(s => s.Price) != 0 ? store.Min(s => s.Price) : 0,
                    StoresNumber = store.Count() != 0 ? store.Count() : 0
                })
                .Select(stat => stat);


        }

        #endregion

    }
}
